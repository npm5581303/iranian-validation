import { validateOrReject } from "class-validator";
import { IsIranianMobile, IsIranianNationalId } from "./src";

class User {
  @IsIranianNationalId()
  nationalId: string;

  @IsIranianMobile()
  mobile: string;
}

const user = new User();
user.nationalId = "4959924446";
user.mobile = "09185565560";

validateOrRejectExample(user);
async function validateOrRejectExample(input) {
  try {
    await validateOrReject(input);
  } catch (errors) {
    console.error(errors);
  }
}
