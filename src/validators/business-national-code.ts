import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "businessNationalId", async: false })
export class BusinessNationalId implements ValidatorConstraintInterface {
  constructor(test: boolean) {
    if (test) this.validate = this.validateTest;
  }
  validate(text: string) {
    if (text.length !== 11) {
      return false;
    }
    const chars = text.split("");
    const controlNum = +(chars.pop() || "0");
    const numbers = chars.map((d) => +d);
    const x = numbers[chars.length - 1] + 2;
    const coefficients = [29, 27, 23, 19, 17, 29, 27, 23, 19, 17];
    let result =
      numbers.reduce((a, b, i) => (b + x) * coefficients[i] + a, 0) % 11;

    if (result === 10) {
      result = 0;
    }

    return result === controlNum;
  }
  validateTest(text: string) {
    if (text.length !== 11) {
      return false;
    }
    return true;
  }

  defaultMessage() {
    return "کدملی شرکت معتبر نمی باشد.";
  }
}

export function IsIranianBusinessNationalId(
  validationOptions?: ValidationOptions & {
    testValidation?: boolean;
  }
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsIranianBusinessNationalId",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: new BusinessNationalId(validationOptions?.testValidation),
    });
  };
}
