import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "iranBankCart", async: false })
export class IranBankCart implements ValidatorConstraintInterface {
  validate(text: string) {
    if (!/^\d{16}$/.test(text)) {
      return false;
    }

    let sum = 0;

    for (let position = 1; position <= 16; position++) {
      let temp = +text[position - 1];
      temp = position % 2 === 0 ? temp : temp * 2;
      temp = temp > 9 ? temp - 9 : temp;

      sum += temp;
    }

    return sum % 10 === 0;
  }

  defaultMessage() {
    return "شماره کارت بانکی معتبر نمی باشد.";
  }
}

export function IsIranianBankCart(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsIranianBankCart",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IranBankCart,
    });
  };
}
