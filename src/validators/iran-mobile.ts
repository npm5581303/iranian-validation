import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "iranMobile", async: false })
export class IranMobile implements ValidatorConstraintInterface {
  validate(text: string) {
    return (
      /^(((98)|(\+98)|(0098)|0)(9){1}[0-9]{9})+$/.test(text) ||
      /^(9){1}[0-9]{9}$/.test(text)
    );
  }

  defaultMessage() {
    return "شماره موبایل معتبر نمی باشد.";
  }
}

export function IsIranianMobile(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsIranianMobile",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IranMobile,
    });
  };
}
