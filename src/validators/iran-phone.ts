import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "iranPhone", async: false })
export class IranPhone implements ValidatorConstraintInterface {
  validate(text: string) {
    return /^(0[1-9]{2})[2-9][0-9]{7}$/.test(text);
  }

  defaultMessage() {
    return "شماره تلفن معتبر نمی باشد.";
  }
}

export function IsIranianCellPhone(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsIranianCellPhone",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IranPhone,
    });
  };
}
