import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "iranBankCart", async: false })
export class IranPostalCode implements ValidatorConstraintInterface {
  validate(text: string) {
    return /\b(?!(\d)\1{3})[13-9]{4}[1346-9][013-9]{5}\b/.test(text);
  }

  defaultMessage() {
    return "کدپستی معتبر نمی باشد.";
  }
}

export function IsIranianPostalCode(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsIranianPostalCode",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IranPostalCode,
    });
  };
}
