import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "melliId", async: false })
export class MelliId implements ValidatorConstraintInterface {
  validate(text: string) {
    let sum = 0;
    const chars = text.split("");
    for (let i = 0; i < 9; i += 1) sum += +chars[i] * (10 - i);
    let lastDigit: any = null;
    const remainder = sum % 11;
    lastDigit = remainder < 2 ? remainder : 11 - remainder;
    return +chars[9] === lastDigit;
  }

  defaultMessage() {
    return "کدملی معتبر نمی باشد.";
  }
}

export function IsIranianNationalId(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsIranianNationalId",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: MelliId,
    });
  };
}
