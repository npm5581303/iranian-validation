import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "notPersian", async: false })
export class NotPersian implements ValidatorConstraintInterface {
  validate(text: string) {
    return !/[\u{600}-\u{6FF}]/u.test(text);
  }

  defaultMessage() {
    return "نباید شامل حروف و اعداد فارسی باشد.";
  }
}

export function IsNotPersianAlphaNum(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsNotPersianAlphaNum",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: NotPersian,
    });
  };
}
