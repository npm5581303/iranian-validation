import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "persianAlpha", async: false })
export class PersianAlpha implements ValidatorConstraintInterface {
  validate(text: string) {
    return /^[\u{600}-\u{6FF}\u{200c}\u{064b}\u{064d}\u{064c}\u{064e}\u{064f}\u{0650}\u{0651}\s]+$/u.test(
      text
    );
  }

  defaultMessage() {
    return "باید شامل حروف فارسی باشد.";
  }
}

export function IsPersianAlphabet(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsPersianAlphabet",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: PersianAlpha,
    });
  };
}
