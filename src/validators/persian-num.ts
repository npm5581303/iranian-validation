import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

@ValidatorConstraint({ name: "persianNum", async: false })
export class PersianNumber implements ValidatorConstraintInterface {
  validate(text: string) {
    return /^[\u{6F0}-\u{6F9}]+$/u.test(text);
  }

  defaultMessage() {
    return "باید اعداد فارسی باشد";
  }
}

export function IsPersianNumber(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsPersianNumber",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: PersianNumber,
    });
  };
}
