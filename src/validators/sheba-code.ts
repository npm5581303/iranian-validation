import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from "class-validator";

function mod97(string: string) {
  let checksum: string | number = string.slice(0, 2);
  let fragment: string;

  for (let offset = 2; offset < string.length; offset += 7) {
    fragment = String(checksum) + string.substring(offset, offset + 7);
    checksum = parseInt(fragment, 10) % 97;
  }

  return checksum;
}

@ValidatorConstraint({ name: "sheba", async: false })
export class Sheba implements ValidatorConstraintInterface {
  validate(text: string) {
    // Check Characters
    if (!/^IR\d{24}$/.test(text)) return false;

    // Match And Capture (0) The Country Code, (1) The Check Digits, And (3) The Rest
    const code = text.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/);

    // Check Syntax And Length
    if (!code) {
      return false;
    }

    // Rearrange country Code & Check Digits, Convert Chars To Ints
    const digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, (letter) => {
      return String(letter.charCodeAt(0) - 55);
    });

    // Final Check
    return mod97(digits) === 1;
  }

  defaultMessage() {
    return "شماره شبا معتبر نمی باشد.";
  }
}

export function IsShebaCode(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: "IsShebaCode",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: Sheba,
    });
  };
}
